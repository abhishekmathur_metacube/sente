package URLStatusTests;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Utils.UtilityClass;
// test GIT

public class bit {
	InternetExplorerDriver driver;	
	String className =  this.getClass().getSimpleName();
	String setClient = "";
	UtilityClass u = new UtilityClass();
	
	
	@BeforeTest // starting point of Test
	public void openConnection() throws InterruptedException
	{
		  System.setProperty("webdriver.ie.driver" , "D:\\IEDriverServer\\IEDriverServer.exe");
		  driver = new InternetExplorerDriver();
		  driver.get("http://home.sentebase.com");
		  setClient=u.openConnection(driver);
		  
	}
	
	
	@AfterMethod 
	public void afterMethod(ITestResult testResult) throws IOException 
	{
		u.takeScreenShotOnFailure(testResult, className, driver);
	
	} 
	
	
	@BeforeMethod
	public void utils()
	{
		
	}
		
	
	@AfterTest //end point of test
	public void closeConnection()
	{
		driver.close();
	}
		
	
	@Test(priority = 0)
	public void customerRequest() 
	{
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get(setClient+"bit/customerrequest.aspx");
		  String actualButton = driver.findElement(By.id("btnNewRequest")).getAttribute("value");
		  String expectedButton= "New Request";
		  Assert.assertEquals(actualButton, expectedButton);
		  	  
    }
	
	@Test(priority = 1)
	public void assetUpdateSelectAsset() 
	{
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get(setClient+"bit/AssetUpdateSelectAsset.aspx");
		  String actualButton = driver.findElement(By.id("btnSearch")).getAttribute("value");
		  String expectedButton= "Search";
		  Assert.assertEquals(actualButton, expectedButton);
		  	  
    }
	
	@Test(priority = 2)
	public void assetStatusUpdate() 
	{
		  driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		  driver.get(setClient+"bit/TRMscan/default.aspx");
		  String actualButton = driver.findElement(By.id("btnCancel")).getAttribute("value");
		  String expectedButton= "Cancel";
		  Assert.assertEquals(actualButton, expectedButton);
		  	  
    }
	
}
